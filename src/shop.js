"use strict";

const productPlaceholder = document.getElementById("products");
const productCountPlaceholder = document.querySelector(".products__count");
const paginationContainer = document.querySelector(".products__pages");
const crumbLinks = document.querySelector(".crumbs");
const brandList = document.getElementById("brand");
const categoryList = document.getElementById("type");

let category = "";
let brand = "";
let currentPage = 1;
const itemsPerPage = 9;

const loading = `<div class="products__loading">Loading...</div>`;

async function getProducts(url) {
  try {
    productPlaceholder.innerHTML = loading;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error("Network response was not ok.");
    }
    const data = await response.json();
    productCountPlaceholder.textContent = `${data.length} products found`;
    return data;
  } catch (error) {
    console.error("Fetch error:", error);
    productPlaceholder.innerHTML = `<div class="products__error">Error loading products.</div>`;
    return [];
  }
}

function showProducts(items, page = 1) {
  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const itemsToShow = items.slice(startIndex, endIndex);

  productPlaceholder.innerHTML = itemsToShow
    .map(
      (item) => `
        <div class="products__item">
            <div class="products__item-image">
                <img src="${item.api_featured_image}" alt="product" />
            </div>
            <div class="products__item-info">
                <div class="products__item-title" title="${item.name}">
                    ${item.name}
                </div>
                <div class="products__item-brand" title="${item.name}">
                    ${item.brand}
                </div>
                <div class="products__item-price">$${item.price}</div>
            </div>
            <button type="button" class="products__item-buy">
                Add to cart
            </button>
        </div>
    `
    )
    .join("");
}

function setUpPagination(itemCount, itemsPerPage) {
  paginationContainer.innerHTML = "";
  let pageCount = Math.ceil(itemCount / itemsPerPage);
  for (let i = 1; i <= 6; i++) {
    const btn = document.createElement("button");
    btn.classList.add("products__page");
    btn.textContent = i;
    if (i === currentPage) {
      btn.classList.add("products__page-active");
    }
    btn.addEventListener("click", async () => {
      currentPage = i;
      await updateProductDisplay();
    });
    paginationContainer.appendChild(btn);
  }
}

async function updateProductDisplay() {
  const requestUrl = buildRequestUrl();
  const items = await getProducts(requestUrl);
  showProducts(items);
  setUpPagination(items.length, 9);
}

function buildRequestUrl() {
  let requestUrl = `https://makeup-api.herokuapp.com/api/v1/products.json`;
  let queryParams = [];
  if (brand) queryParams.push(`brand=${encodeURIComponent(brand)}`);
  if (category)
    queryParams.push(`product_type=${encodeURIComponent(category)}`);
  if (queryParams.length) requestUrl += "?" + queryParams.join("&");
  return requestUrl;
}

brandList.addEventListener("click", async (event) => {
  if (event.target.classList.contains("products__link-btn")) {
    if (event.target.textContent === "Any") {
      brand = "";
    } else {
      const newCategory = `<a href="#" class="crumbs__link">${event.target.textContent}</a>`;
      crumbLinks.innerHTML =
        `<a href="#" class="crumbs__link">Categories</a>` +
        newCategory +
        `${category && `<a href="#" class="crumbs__link">${category}</a>`}`;
      brand = event.target.textContent.toLowerCase();
      console.log("brand " + brand);
      currentPage = 1;
      await updateProductDisplay();
    }
  }
});

categoryList.addEventListener("click", async (event) => {
  if (event.target.classList.contains("products__link-btn")) {
    const newCategory = `<a href="#" class="crumbs__link">${event.target.textContent}</a>`;
    crumbLinks.innerHTML =
      `<a href="#" class="crumbs__link">Categories</a>` +
      `${brand && `<a href="#" class="crumbs__link">${brand}</a>`}` +
      newCategory;
    category = event.target.textContent.toLowerCase();
    console.log("category " + category);
    currentPage = 1;
    await updateProductDisplay();
  }
});

updateProductDisplay();
