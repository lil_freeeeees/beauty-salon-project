"use strict";

const slidersWrapper = document.getElementById("scroller");
console.log(document.querySelector(".gallery__list"));

const sliderContent = Array.from(slidersWrapper.children);

sliderContent.forEach((item) => {
  const duplicatedItem = item.cloneNode(true);
  duplicatedItem.setAttribute("aria-hidden", true);

  slidersWrapper.appendChild(duplicatedItem);
});

